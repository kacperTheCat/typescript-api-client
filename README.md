# tiny.cc/pcu3iz - repo

![](gitlab_url.png)

http://spec.openapis.org/oas/v2.0 - openAPI doc

https://github.com/OAI/OpenAPI-Specification/blob/master/examples/v3.0/petstore.yaml - pet store definition yams
https://github.com/OAI/OpenAPI-Specification/blob/master/examples/v2.0/json/petstore.json - pet store definition son
https://editor.swagger.io/ - definitions editor

https://github.com/openapitools/openapi-generator/#16---docker - openAPI generator doc, docker section
https://docs.docker.com/install/ - docker doc
https://petstore.swagger.io/ - petstore service

kacper.one.borowski@gmail.com
