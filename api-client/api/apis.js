"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./petApi"));
const petApi_1 = require("./petApi");
__export(require("./storeApi"));
const storeApi_1 = require("./storeApi");
__export(require("./userApi"));
const userApi_1 = require("./userApi");
class HttpError extends Error {
    constructor(response, body, statusCode) {
        super('HTTP request failed');
        this.response = response;
        this.body = body;
        this.statusCode = statusCode;
        this.name = 'HttpError';
    }
}
exports.HttpError = HttpError;
exports.APIS = [petApi_1.PetApi, storeApi_1.StoreApi, userApi_1.UserApi];
//# sourceMappingURL=apis.js.map