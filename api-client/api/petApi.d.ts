/// <reference types="node" />
import http = require('http');
import { ApiResponse } from '../model/apiResponse';
import { Pet } from '../model/pet';
import { Authentication } from '../model/models';
import { ApiKeyAuth, OAuth } from '../model/models';
import { RequestFile } from './apis';
export declare enum PetApiApiKeys {
    api_key = 0
}
export declare class PetApi {
    protected _basePath: string;
    protected defaultHeaders: any;
    protected _useQuerystring: boolean;
    protected authentications: {
        'default': Authentication;
        'petstore_auth': OAuth;
        'api_key': ApiKeyAuth;
    };
    constructor(basePath?: string);
    useQuerystring: boolean;
    basePath: string;
    setDefaultAuthentication(auth: Authentication): void;
    setApiKey(key: PetApiApiKeys, value: string): void;
    accessToken: string;
    addPet(body: Pet, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.IncomingMessage;
        body?: any;
    }>;
    deletePet(petId: number, apiKey?: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.IncomingMessage;
        body?: any;
    }>;
    findPetsByStatus(status: Array<'available' | 'pending' | 'sold'>, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.IncomingMessage;
        body: Array<Pet>;
    }>;
    findPetsByTags(tags: Array<string>, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.IncomingMessage;
        body: Array<Pet>;
    }>;
    getPetById(petId: number, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.IncomingMessage;
        body: Pet;
    }>;
    updatePet(body: Pet, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.IncomingMessage;
        body?: any;
    }>;
    updatePetWithForm(petId: number, name?: string, status?: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.IncomingMessage;
        body?: any;
    }>;
    uploadFile(petId: number, additionalMetadata?: string, file?: RequestFile, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.IncomingMessage;
        body: ApiResponse;
    }>;
}
