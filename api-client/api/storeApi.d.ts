/// <reference types="node" />
import http = require('http');
import { Order } from '../model/order';
import { Authentication } from '../model/models';
import { ApiKeyAuth } from '../model/models';
export declare enum StoreApiApiKeys {
    api_key = 0
}
export declare class StoreApi {
    protected _basePath: string;
    protected defaultHeaders: any;
    protected _useQuerystring: boolean;
    protected authentications: {
        'default': Authentication;
        'api_key': ApiKeyAuth;
    };
    constructor(basePath?: string);
    useQuerystring: boolean;
    basePath: string;
    setDefaultAuthentication(auth: Authentication): void;
    setApiKey(key: StoreApiApiKeys, value: string): void;
    deleteOrder(orderId: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.IncomingMessage;
        body?: any;
    }>;
    getInventory(options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.IncomingMessage;
        body: {
            [key: string]: number;
        };
    }>;
    getOrderById(orderId: number, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.IncomingMessage;
        body: Order;
    }>;
    placeOrder(body: Order, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.IncomingMessage;
        body: Order;
    }>;
}
