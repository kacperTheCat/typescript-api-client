"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ApiResponse {
    static getAttributeTypeMap() {
        return ApiResponse.attributeTypeMap;
    }
}
ApiResponse.discriminator = undefined;
ApiResponse.attributeTypeMap = [
    {
        "name": "code",
        "baseName": "code",
        "type": "number"
    },
    {
        "name": "type",
        "baseName": "type",
        "type": "string"
    },
    {
        "name": "message",
        "baseName": "message",
        "type": "string"
    }
];
exports.ApiResponse = ApiResponse;
//# sourceMappingURL=apiResponse.js.map