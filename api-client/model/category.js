"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Category {
    static getAttributeTypeMap() {
        return Category.attributeTypeMap;
    }
}
Category.discriminator = undefined;
Category.attributeTypeMap = [
    {
        "name": "id",
        "baseName": "id",
        "type": "number"
    },
    {
        "name": "name",
        "baseName": "name",
        "type": "string"
    }
];
exports.Category = Category;
//# sourceMappingURL=category.js.map