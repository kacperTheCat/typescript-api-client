"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Tag {
    static getAttributeTypeMap() {
        return Tag.attributeTypeMap;
    }
}
Tag.discriminator = undefined;
Tag.attributeTypeMap = [
    {
        "name": "id",
        "baseName": "id",
        "type": "number"
    },
    {
        "name": "name",
        "baseName": "name",
        "type": "string"
    }
];
exports.Tag = Tag;
//# sourceMappingURL=tag.js.map