export declare class User {
    'id'?: number;
    'username'?: string;
    'firstName'?: string;
    'lastName'?: string;
    'email'?: string;
    'password'?: string;
    'phone'?: string;
    'userStatus'?: number;
    static discriminator: string | undefined;
    static attributeTypeMap: Array<{
        name: string;
        baseName: string;
        type: string;
    }>;
    static getAttributeTypeMap(): {
        name: string;
        baseName: string;
        type: string;
    }[];
}
