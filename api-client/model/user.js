"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class User {
    static getAttributeTypeMap() {
        return User.attributeTypeMap;
    }
}
User.discriminator = undefined;
User.attributeTypeMap = [
    {
        "name": "id",
        "baseName": "id",
        "type": "number"
    },
    {
        "name": "username",
        "baseName": "username",
        "type": "string"
    },
    {
        "name": "firstName",
        "baseName": "firstName",
        "type": "string"
    },
    {
        "name": "lastName",
        "baseName": "lastName",
        "type": "string"
    },
    {
        "name": "email",
        "baseName": "email",
        "type": "string"
    },
    {
        "name": "password",
        "baseName": "password",
        "type": "string"
    },
    {
        "name": "phone",
        "baseName": "phone",
        "type": "string"
    },
    {
        "name": "userStatus",
        "baseName": "userStatus",
        "type": "number"
    }
];
exports.User = User;
//# sourceMappingURL=user.js.map