require('isomorphic-fetch')

fetch('https://petstore.swagger.io/v2/pet/1234567')
  .then(response => response.json())
  .then(data => console.log('Fetch: ', data.name)
  )

const petDataToUpdate = {
  id: 1234567,
  name: 'Gorilla',
  photoUrls: ['http://img.burrard-lucas.com/rwanda/normal/gorilla_stare.jpg']
}

fetch('https://petstore.swagger.io/v2/pet/', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(petDataToUpdate),
})
  .then((response) => response.json())
  .then((data) => {
    console.log('Success:', data)
  })
  .catch((error) => {
    console.error('Error:', error)
  })


