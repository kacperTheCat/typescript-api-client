import { PetApi } from './api-client/api/petApi'
import { Pet } from './api-client/model/pet'

const petApi = new PetApi()

petApi.getPetById(1234567).then(res => res.body.name)

const body: Pet = {
  id: 1234567,
  name: 'Gorilla',
  photoUrls: ['http://img.burrard-lucas.com/rwanda/normal/gorilla_stare.jpg']
}

petApi.updatePet({name: 'Gorilla', photoUrls: ['url']})